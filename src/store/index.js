import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  state: {
    logged: false
  },
  getters: {
    isLogged: state => {
      return state.logged
    }
  },
  mutations: {
    updatedLogged (state, value) {
      state.logged = value
    }
  },
  strict: debug
})
